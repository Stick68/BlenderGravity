
import json
from tkinter import *
import customtkinter as ctk
import subprocess
import os

infos_planetes = {}  # Variable qui va stocker les données des planètes.
planetes = []        # Variables qui va stocker les noms des planètes
nom_fichier_json = "donnees_planetes.json" # Obtenez le chemin complet du fichier

def vider_données ():  # fonction pour vider les données 
    global infos_planetes, planetes
    infos_planetes = {}
    planetes = []
    nom_fichier_json = "donnees_planetes.json" # Obtenez le chemin complet du fichier ( en principe, c'est le même, si ils sont dans le même dossier)
    with open(nom_fichier_json, "w") as fichier_json:
        json.dump({"planetes": planetes, "infos_planetes": infos_planetes}, fichier_json)

def lancer_blender():  # Fonction qui permet de lancer blender
    # Nom de votre fichier Blender
    nom_fichier_blender = "Terre_lune.blend"

    # Chemin complet vers le fichier Blender
    chemin_fichier_blender = os.path.join(os.path.dirname(__file__), nom_fichier_blender)

    # Chemin vers l'exécutable de Blender propre à chaque pc. A CHANGER !
    chemin_blender = r"C:\Program Files\Blender Foundation\Blender 4.1\blender-launcher.exe"

    # Construire la commande pour ouvrir Blender avec le fichier spécifié
    commande = [chemin_blender, chemin_fichier_blender]

    print(infos_planetes)
    print(planetes)

    # Utilisez ce chemin pour enregistrer le fichier JSON
    nom_fichier_json = "donnees_planetes.json" # Obtenez le chemin complet du fichier
    with open(nom_fichier_json, "w") as fichier_json:
        json.dump({"planetes": planetes, "infos_planetes": infos_planetes}, fichier_json)

    # Lancer Blender avec le fichier .blend
    subprocess.run(commande)


def lancer_solaire():
    # Nom de votre fichier Blender
    nom_fichier_blender = "systeme_solaire.blend"

    # Chemin complet vers le fichier Blender
    chemin_fichier_blender = os.path.join(os.path.dirname(__file__), nom_fichier_blender)

    # Chemin vers l'exécutable de Blender propre à chaque pc. A CHANGER !
    chemin_blender = r"C:\Program Files\Blender Foundation\Blender 4.1\blender-launcher.exe"

    # Construire la commande pour ouvrir Blender avec le fichier spécifié
    commande = [chemin_blender, chemin_fichier_blender]

    # Lancer Blender avec le fichier .blend
    subprocess.run(commande)


def princi():
    ctk.set_appearance_mode("dark")
    ctk.set_default_color_theme("dark-blue")
    formu = ctk.CTk()
    formu.geometry("1000x600")

    frame = ctk.CTkFrame(master=formu)
    frame.pack(pady=30, padx=60, fill="both", expand=True)

    titre = ctk.CTkLabel(master=frame, text="Animation gravitationnelle", font=("Arial", 40))
    titre.pack(pady=20, padx=20)

    nombre_planete = ctk.CTkEntry(master=frame, placeholder_text="Nombre planète")
    nombre_planete.pack(pady=30, padx=10)

    def commencer():
        vider_données()
        formulaire(0, int(nombre_planete.get()))

    button = ctk.CTkButton(master=frame, text="Commencer", width=100, height=30, command=commencer)
    button.pack(pady=10, padx=10)

    bouton2 = ctk.CTkButton(master=frame, text="Animation Blender", width=100, height=30, command=lancer_blender)
    bouton2.pack(pady=10, padx=10)

    button3 = ctk.CTkButton(master=frame, text="Le Système Solaire", width=100, height=30, command=lancer_solaire)
    button3.pack(pady=10, padx=10)

    formu.mainloop()


def formulaire(num_planete, nombre_planetes):
    ctk.set_appearance_mode("dark")
    ctk.set_default_color_theme("dark-blue")
    formu = ctk.CTk()
    formu.geometry("1000x600")

    frame = ctk.CTkFrame(master=formu)
    frame.pack(pady=30, padx=60, fill="both", expand=True)

    titre = ctk.CTkLabel(master=frame, text="Animation gravitationnelle", font=("Arial", 30))
    titre.pack(pady=20, padx=20)

    # ---Formulaire------#

    nom = ctk.CTkEntry(master=frame, width= 300, placeholder_text="Nom planète")
    nom.pack(pady=5, padx=10)

    masse = ctk.CTkEntry(master=frame, width= 300, placeholder_text="Masse planète (Kg)")
    masse.pack(pady=5, padx=10)

    vitesse = ctk.CTkEntry(master=frame, width= 300, placeholder_text="Vitesse planète (m/s) 0 pour la premiere")
    vitesse.pack(pady=5, padx=10)

    Positionx = ctk.CTkEntry(master=frame, width= 300, placeholder_text="Position X planète (m) 0 pour la premiere")
    Positionx.pack(pady=5, padx=10)

    Positiony = ctk.CTkEntry(master=frame, width= 300, placeholder_text="Position Y planète (m) 0 pour la premiere")
    Positiony.pack(pady=5, padx=10)

    rayon = ctk.CTkEntry(master=frame, width= 300, placeholder_text="rayon planète (Km)")
    rayon.pack(pady=5, padx=10)

    def enregistrer_informations():
        nom_planete = nom.get()
        planetes.append(nom_planete)
        infos_planetes[nom_planete] = {
            "Masse": masse.get(),
            "Vitesse": vitesse.get(),
            "Position": [Positionx.get(), Positiony.get(),0],
            "Rayon": rayon.get()
        }
        nom_fichier_json = "donnees_planetes.json" # Obtenez le chemin complet du fichier
        with open(nom_fichier_json, "w") as fichier_json:
            json.dump({"planetes": planetes, "infos_planetes": infos_planetes}, fichier_json)
        
        formu.destroy()
        if num_planete + 1 < nombre_planetes:
            formulaire(num_planete + 1, nombre_planetes)


    button = ctk.CTkButton(master=frame, text="Continuer", width=80, height=30, command=enregistrer_informations)
    button.pack(pady=20, padx=10)

    formu.mainloop()

princi()
