## Protocole d'utilisation 

### Windows
- ### Installation : 
    - Installer python 3 ou supérieur 
    lien ici : ([ici](https://www.python.org/downloads/windows/))
    - Installer Blender : 
    lien ici : ([ici](https://www.blender.org/download/))
    - Installer les modules nécessaires : 
    "pip install -r requirements.txt"
- ### Execution : 
    -Depuis  le repertoire "sources" : blenderGravity.py

- ### Cas erreur possible (si un fichier n'est pas trouvé)
    - Vérifier le chemin d'accès au fichier blender-launcher de votre poste et modifier les lignes 28 et 53 en conséquence
    C:\Program Files\Blender Foundation\Blender 4.1\blender-launcher.exe 

